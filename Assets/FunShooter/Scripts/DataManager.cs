﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

[AddComponentMenu("FunShooterVR/Managers/DataManager")]
// Main Logic
public partial class DataManager : MonoBehaviour
{
    public static DataManager instance;

    private void Awake()
    {
        if(instance != null)
        {
            DestroyImmediate(this);
            return;
        }
        else instance = this;
    }
}

// Saver logic
public partial class DataManager : MonoBehaviour
{
    public void SaveData<T>(ref string _dataContainer, T _object)
    {
        _dataContainer = ObjectToJson(_object);
    }

    public List<string> listString = new List<string>();

    public void SaveFeatures(List<Feature> _features, ref List<string> _listString)
    {
        
    }
}

// Loader logic
public partial class DataManager : MonoBehaviour
{
    public void LoadDataFromPlayerInventoryData(ref Player_Inventory_Data _playerInventoryData)
    {

    }

    public Object LoadObject<T>(string _object)
    {
        try
        {
            var resource = Resources.Load(_object, typeof(T));
            return resource;
        }
        catch (System.Exception e)
        {
            Debug.LogError("FunShooterDebugError: " + e);
        }
        
        return null;
    }
}

// Jsons logic
public partial class DataManager : MonoBehaviour
{
    public static string ObjectToJson<T>(T _object)
    {
        return JsonConvert.SerializeObject(_object);
    }

    public void PopulateObjFromString<T>(string _data, ref T _object)
    {
        JsonConvert.PopulateObject(_data, _object);
    }
}