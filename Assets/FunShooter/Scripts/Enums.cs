﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;

public enum AmmoType
{
    Ordinary,
    IncreasedPenetration,
    ArmorPiercing,
    Tracer,
    Incendiary,
    Expancive,
    Shotguns,
    Chemical,
    Traumatic,
    Propaganda,
    Homing
}

public enum ModType
{
    Base,
    Barrel,
    Mag,
    Scope,
    UnderBarrel,
    Chamber,
    Grip,
    Stock,
    AmmoToggle,
    Silencer
}

public enum FeatureType
{
    Base,
    Barrel,
    Mag,
    Scope,
    UnderBarrel,
    Chamber,
    Grip,
    Stock,
    AmmoToggle,
    Silencer,
    AttachPoints
}