﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("FunShooterVR/Managers/GameManager")]
public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public string currPlayerName_Test; // for test player inventory data and player data scripts
    public Player_Inventory_Data playerInventoryData;

    private void Awake()
    {
        if (instance != null)
        {
            DestroyImmediate(gameObject);
            return;
        }
        else instance = this;

        if(playerInventoryData == null)
        {
            playerInventoryData = Resources.Load(currPlayerName_Test, typeof(Player_Inventory_Data)) as Player_Inventory_Data;
        }
    }

    
}
