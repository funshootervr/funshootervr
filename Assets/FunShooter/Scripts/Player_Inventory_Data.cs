﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// SO_Mod как скриптуемый обьект предназначен только для хранения информации о текущем Mod
[CreateAssetMenu(fileName = "Player_Inventory_Data", menuName = "FunShooter/Data/Player_Inventory_Data")]
public class Player_Inventory_Data : ScriptableObject
{
    public List<string> weapons;
}

[System.Serializable]
public class Player_Data
{
    public string nickName;
    public string playerInventoryData;
}