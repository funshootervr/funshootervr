﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

public class Feature : MonoBehaviour
{ 
    public string FeatureName;
    public FeatureType featureType;
}
