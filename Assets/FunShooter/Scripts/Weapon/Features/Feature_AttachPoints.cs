﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor.Events;
using UnityEngine;
using UnityEngine.Events;
using System;

[Serializable]
public class FeatureEvents
{

}

public class Feature_AttachPoints : Feature
{
    public List<AttachPointsGroup> attachPointsGroups;

    Mod mod;

    private void Awake()
    {
        if(mod == null)
        {
            mod = GetComponent<Mod>();
            foreach(AttachPointsGroup item in attachPointsGroups)
            {
                foreach(AttachPoint point in item.attachPoints)
                {
                    point.mod = mod;
                }
            }
        }
    }

    private void Start()
    {
        if(attachPointsGroups.Count > 0)
        {
            foreach(AttachPointsGroup item in attachPointsGroups)
            {
                mod.modEvents.onChangeInHand.AddListener(item.ShowPoints);
            }
        }
    }

    void ShowAttachPoints(bool _show)
    {

    }
}
