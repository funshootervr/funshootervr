﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

public class Feature_Mag : Feature
{
    public float capacity;
    public float speedReload;

    [SerializeField] float currAmmoCount;

    public float CurrAmmoCount
    { 
        get => currAmmoCount; 
        set
        {
            if (value + currAmmoCount <= capacity)
                currAmmoCount = value;
            else
                currAmmoCount = capacity;
        } 
    }

}
