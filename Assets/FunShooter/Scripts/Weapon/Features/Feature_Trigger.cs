﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

#region featureEvents

[Serializable] public class TriggerIsPressed : UnityEvent<bool> { }

[Serializable]
public class Feature_Trigger_Events // ивенты для классов зависимых от изминения состояния некоторых переменных 
{
    public TriggerIsPressed triggerIsPressed;
}

#endregion
public class Feature_Trigger : Feature
{
    private bool isPressed;

    public Feature_Trigger_Events featureEvents;

    public bool IsPressed 
    { 
        get => isPressed; 
        set
        {
            isPressed = value;
            featureEvents.triggerIsPressed.Invoke(value);
        }
    }
}
