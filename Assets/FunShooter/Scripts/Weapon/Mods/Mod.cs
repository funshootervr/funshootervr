﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using Newtonsoft.Json;
using System.Linq;
using UnityEngine.Events;

[Serializable]
#if UNITY_EDITOR
[ExecuteInEditMode]
#endif
public class Mods_Variable_data
{
    public string modName;
    public string dataInString;
    public Vector3 localPosition;
    public Vector3 localRotation;
    public Vector3 localScale;

    public int numOfParentSocket;

    public List<Mods_Variable_data> mods;

    public Mods_Variable_data() { }

    public Mods_Variable_data(string _data)
    {
        JsonConvert.PopulateObject(_data, this);
    }
}

#region modEvents

[Serializable] public class OnChangeIsParent : UnityEvent<bool> { }
[Serializable] public class OnChangeInHand : UnityEvent<bool> { }
[Serializable] public class OnChangeInRightHand : UnityEvent<bool> { }
[Serializable] public class OnChangeInLeftHand : UnityEvent<bool> { }

[Serializable]
public class ModEvents // ивенты для классов зависимых от изминения состояния некоторых переменных 
{
    public OnChangeIsParent onChangeIsParent;
    public OnChangeInHand onChangeInHand;
    public OnChangeInRightHand onChangeInRightHand;
    public OnChangeInLeftHand onChangeInLeftHand;
}

#endregion

[AddComponentMenu("FunShooterVR/Weapons/Mod")]
public class Mod : MonoBehaviour
{
    #region privateVariables

    [SerializeField] GameObject modParent;
    [SerializeField] bool isParent;
    [SerializeField] bool inRightHand;
    [SerializeField] bool inLeftHand;
    [SerializeField] bool inHand;

    #endregion

    #region publicVariables

    public Mods_Variable_data data = new Mods_Variable_data();
    public List<Feature> features;
    public ModEvents modEvents;

    #endregion

    #region Get/Set'ers 

    public bool IsParent
    {
        get => isParent;
        set
        {
            isParent = value;
            modEvents.onChangeIsParent.Invoke(value);
        }
    }
    public bool InHand { 
        get => inHand; 
        set
        {
            inHand = value;
            modEvents.onChangeInHand.Invoke(value);
            if(GetComponentsInChildren<Mod>().Length > 0)
            {
                foreach(Mod item in GetComponentsInChildren<Mod>())
                {
                    if (item != this)
                    {
                        item.InHand = value;
                    }
                }
            }
        }
    }
    public bool InLeftHand
    {
        get => inLeftHand;
        set
        {
            inLeftHand = value;
            modEvents.onChangeInLeftHand.Invoke(value);
            CheckInHand();
            UnParentObject();
        }
    }
    public bool InRightHand
    {
        get => inRightHand;
        set
        {
            inRightHand = value;
            modEvents.onChangeInRightHand.Invoke(value);
            CheckInHand();
            UnParentObject();
        }
    }
    public GameObject ModParent
    {
        get => modParent;
        set
        {
            modParent = value;
            //transform.SetParent(value.transform);
        }
    }

    #endregion

    private void Start()
    {
        ReloadListFeatures();
        CheckIsParent();
        InHand = false; // for hide all attach points;
    }

    void CheckInHand()
    {
        if(inRightHand || inLeftHand)
        {
            InHand = true;
        }
        else if(!inRightHand && !inLeftHand)
        {
            InHand = false;
        }
    }

    void UnParentObject()
    {
        if(InHand && ModParent != null)
        {
            transform.SetParent(null);
            modParent = null;
        }
    }

    [ContextMenu("Check Is Parent")]
    void CheckIsParent()
    {
        if (transform.parent == null)
        {
            IsParent = true;
            SetModToWeapon();
        }
        else
        {
            IsParent = false;
            ModParent = transform.parent.gameObject;
        }
    }

    void SetModToWeapon()
    {
        GameObject weapon = Instantiate(DataManager.instance.LoadObject<GameObject>("Weapon")) as GameObject;
        weapon.transform.position = CalculateCenterPivot();
        transform.SetParent(weapon.transform);
    }

    Vector3 CalculateCenterPivot()
    {
        Bounds bounds = new Bounds();
        foreach(MeshFilter filter in GetComponentsInChildren<MeshFilter>())
        {
            bounds.Encapsulate(filter.mesh.bounds);
        }
        return bounds.center;
    }

    [ContextMenu("Reload List Features")]
    void ReloadListFeatures()
    {
        features.Clear();
        features.AddRange(GetComponents<Feature>());
    }

    public virtual void Init(GameObject _gameObject, Vector3 localPos, Vector3 localRot, Vector3 localScl, GameObject _parent)
    {
        ModParent = _parent;
        _gameObject.transform.SetParent(_parent.transform);
        _gameObject.transform.localPosition = localPos;
        _gameObject.transform.localEulerAngles = localRot;
        _gameObject.transform.localScale = localScl;
    }

#if UNITY_EDITOR
    #region developScripts
    [ContextMenu("Toggle IsParent")]
    public void SetIsParentToggle()
    {
        IsParent = !IsParent;
    }

    [ContextMenu("Toggle InRightHand")]
    public void SetInRightHandToggle()
    {
        InRightHand = !InRightHand;
    }

    [ContextMenu("Toggle InLeftHand")]
    public void SetInLeftHandToggle()
    {
        InLeftHand = !InLeftHand;
    }

    [ContextMenu("Set All Attach Points")]
    void SetAllAttachPoints()
    {
        var AttachFeature = GetComponent<Feature_AttachPoints>();
        AttachFeature.attachPointsGroups = new List<AttachPointsGroup>();
        if(GetComponentsInChildren<AttachPointsGroup>().Length > 0)
        {
            foreach(AttachPointsGroup group in GetComponentsInChildren<AttachPointsGroup>())
            {
                group.attachPoints = new List<AttachPoint>(group.GetComponentsInChildren<AttachPoint>());
                group.attachPointsGroupId = group.name;
                int i = 1;
                foreach(AttachPoint point in group.attachPoints)
                {
                    point.attachPointId = group.attachPointsGroupId + "_" + i++;
                    point.name = "AttachPoint";
                }
                AttachFeature.attachPointsGroups.Add(group);
            }
        }
    }
    #endregion
#endif
}

