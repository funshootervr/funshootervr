﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttachPoint : MonoBehaviour
{
    public string attachPointId;
    [SerializeField] GameObject currChild;
    [SerializeField] bool hasChild;
    private bool showTriggerCollider;

    public Mod mod;

    public GameObject CurrChild 
    { 
        get => currChild; 
        set
        {
            currChild = value;
            if(value != null)
            {
                HasChild = true;
            }
        }
    }

    public bool HasChild
    { 
        get => hasChild; 
        set
        {
            hasChild = value;
            if(value)
            {
                transform.parent.GetComponent<AttachPointsGroup>().HasChild = true;
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if(!HasChild)
        {
            if (other.GetComponent<AttachPoint>() != null)
            {
                AttachPoint point = other.GetComponent<AttachPoint>();
                if (mod.InHand && !point.mod.InHand)
                {
                    NormalaizePositionChild(point.mod.transform, point.transform);
                    point.mod.transform.SetParent(mod.GetComponentInParent<Weapon>().transform);
                    point.mod.InHand = true;
                    point.mod.ModParent = mod.gameObject;
                    //HasChild = true;
                }
            }
        }
    }

    void NormalaizePositionChild(Transform _child, Transform _attachPoint)
    {
        _child.transform.position = transform.position + (_child.transform.position - _attachPoint.transform.position);
    }

    void NormalaizeRotationChild(Transform _child, AttachPoint _attachPoint)
    {
        
    }
}