﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttachPointsGroup : MonoBehaviour
{
    public string attachPointsGroupId;
    public List<AttachPoint> attachPoints;
    [SerializeField] bool hasChild;

    public bool HasChild 
    { 
        get => hasChild; 
        set
        {
            hasChild = value;
            if(value && attachPoints.Count > 0)
            {
                foreach(AttachPoint item in attachPoints)
                {
                    if(item.HasChild != value)
                    {
                        item.HasChild = value;
                    }
                }
            }
        }
    }

    private void Start()
    {
        ShowPoints(false);
    }

    public void ShowPoints(bool _show)
    {
        if(attachPoints.Count > 0)
        {
            foreach(AttachPoint item in attachPoints)
            {
                item.GetComponent<Renderer>().enabled = _show;
            }
        }
    }
}
