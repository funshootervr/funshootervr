﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("FunShooterVR/Weapons/Weapon")]
public class Weapon : MonoBehaviour
{
    public string weaponName;
    public List<Mod> mods;

    [SerializeField] List<Feature> allFeaturesInWeapon = new List<Feature>();

    internal void Init(Mod _firstMod)
    {
        RealoadAllFeaturesList();
    }

    [ContextMenu("Reaload mods")]
    void RealoadAllFeaturesList()
    {
        mods = new List<Mod>(GetComponentsInChildren<Mod>());
        allFeaturesInWeapon = new List<Feature>(GetComponentsInChildren<Feature>());
        RecalculateWeaponParameters();
    }

    [ContextMenu("Recalculate Weapon Parameters")]
    public void RecalculateWeaponParameters()
    {

    }
}
