﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParentForSOLogic : MonoBehaviour
{
    public SO_Logic testLogic;

    public string color;
    public int ammo;

    private void Awake()
    {
        if(testLogic != null)
        {
            testLogic.Init(this, gameObject);
        }
    }

    private void OnMouseDown()
    {
        testLogic.MainAction();    
    }
}
