﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "FunShooter/Only Tests/New SO_Logic", fileName = "New SO_Logic")]
public class SO_Logic : ScriptableObject
{
    public List<SO_LogicType> logicTypes;

    [SerializeField] ParentForSOLogic parentForSOLogic;
    [SerializeField] GameObject parent;
    

    public Color color;
    public int minChangeAmmo, maxChangeAmmo;
    [Range(-100,100)]public int changeAmmo;

    public void Init(ParentForSOLogic _parentForSOLogic, GameObject _parent)
    {
        parentForSOLogic = _parentForSOLogic;
        parent = _parent;
    }

    internal void MainAction()
    {
        foreach(SO_LogicType item in logicTypes)
        {
            Action(item);
        }
    }

    void Action(SO_LogicType type)
    {
        switch (type)
        {
            case SO_LogicType.ChangeColor:
                ChangeColor();
                break;
            case SO_LogicType.ChangeAmmoCount:
                ChangeAmmoCount();
                break;
        }
    }

    void ChangeColor()
    {
        parent.GetComponent<Renderer>().material.color = color;
        parentForSOLogic.color = color.ToString();
        Debug.Log("ChangeColor");
    }

    void ChangeAmmoCount()
    {
        parentForSOLogic.ammo += changeAmmo;
        Debug.Log("ChangeAmmoCount");
    }
}

public enum SO_LogicType
{
    ChangeColor,
    ChangeAmmoCount
}
