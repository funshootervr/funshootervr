﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(SO_Logic))]
[CanEditMultipleObjects]

public class SO_LogicEditor : Editor
{
	SO_Logic subject;
	SerializedProperty logicTypes;
	SerializedProperty color;
	SerializedProperty changeAmmo;
	SerializedProperty parentForSOLogic;
	SerializedProperty parent;
	SerializedProperty minChangeAmmo;
	SerializedProperty maxChangeAmmo;

	//Передаём этому скрипту компонент и необходимые в редакторе поля
	void OnEnable()
	{
		subject = target as SO_Logic;

		logicTypes = serializedObject.FindProperty("logicTypes");
		parentForSOLogic = serializedObject.FindProperty("parentForSOLogic");
		parent = serializedObject.FindProperty("parent");

		color = serializedObject.FindProperty("color");
		changeAmmo = serializedObject.FindProperty("changeAmmo");
		minChangeAmmo = serializedObject.FindProperty("minChangeAmmo");
		maxChangeAmmo = serializedObject.FindProperty("maxChangeAmmo");

	}

	//Переопределяем событие отрисовки компонента
	public override void OnInspectorGUI()
	{
		//Метод обязателен в начале. После него редактор компонента станет пустым и
		//далее мы с нуля отрисовываем его интерфейс.
		serializedObject.Update();

		//Вывод в редактор полей которые мы не хотим менять
		//EditorGUILayout.PropertyField(parentForSOLogic);
		//EditorGUILayout.PropertyField(parent);
		//Вывод в редактор выпадающего меню с  типом компонента
		 EditorGUILayout.PropertyField(logicTypes, new GUIContent(logicTypes.name), true);

		//Проверка выбранного пункта в выпадающем меню, 
		foreach (SO_LogicType item in subject.logicTypes)
		{
			ShowParams(item);
		}

		//Метод обязателен в конце
		serializedObject.ApplyModifiedProperties();
	}

	void ShowParams(SO_LogicType type)
	{
		switch (type)
		{
			case SO_LogicType.ChangeColor:
				EditorGUILayout.HelpBox("Write here what is doing this variables", MessageType.Info);
				subject.color = EditorGUILayout.ColorField(new GUIContent(color.name), subject.color);
				//EditorGUILayout.EndVertical();
				break;
			case SO_LogicType.ChangeAmmoCount:
				subject.minChangeAmmo = EditorGUILayout.IntField(new GUIContent(minChangeAmmo.name), subject.minChangeAmmo);
				subject.maxChangeAmmo = EditorGUILayout.IntField(new GUIContent(maxChangeAmmo.name), subject.maxChangeAmmo);
				EditorGUILayout.IntSlider(changeAmmo, subject.minChangeAmmo, subject.maxChangeAmmo, new GUIContent("Variable Second"));
				break;
		}
	}
}